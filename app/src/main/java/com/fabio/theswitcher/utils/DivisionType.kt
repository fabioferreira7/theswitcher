package com.fabio.theswitcher.utils

enum class DivisionType(val displayName: String) {
    KITCHEN("Kitchen"),
    LIVING_ROOM("Living room"),
    MASTER_BEDROOM("Master bedroom"),
    GUEST_BEDROOM("Guest's bedroom"),
    CUSTOM("Custom")
}