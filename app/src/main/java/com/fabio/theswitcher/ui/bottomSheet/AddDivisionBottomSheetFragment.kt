package com.fabio.theswitcher.ui.bottomSheet

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.fabio.theswitcher.R
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.databinding.FragmentAddDivisionBottomSheetBinding
import com.fabio.theswitcher.utils.DivisionType
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddDivisionBottomSheetFragment : BottomSheetDialogFragment() , View.OnClickListener {

    lateinit var binding: FragmentAddDivisionBottomSheetBinding
    private val viewModel: AddDivisionBottomSheetViewModel by viewModels()


    companion object {
        const val TAG = "AddDivisionBottomSheetFragment"

        fun newInstance(): AddDivisionBottomSheetFragment {
            return AddDivisionBottomSheetFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddDivisionBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomSheetDialog = dialog as BottomSheetDialog
        val bottomSheet = bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
        setPeekHeight(bottomSheet)
        initListeners()
        observeViewModel()
    }

    // Observe the view model and update the UI accordingly.
    private fun observeViewModel() {
        viewModel.errorMessage.observe(viewLifecycleOwner){
            it?.let { message ->
                // If the message contains the CUSTOM division type, show an error message with the division name.
                if (message.contains(DivisionType.CUSTOM.name)){
                    binding.formAddCustomDivision.tilNewDivisionName.error = getString(R.string.division_already_exists,message.split("/").last())
                }
                // Otherwise, show a snackbar message with the division name and dismiss the bottom sheet.
                else{
                    Snackbar.make(requireActivity().findViewById(android.R.id.content),
                        getString(R.string.division_already_exists,message.split("/").last()), Snackbar.LENGTH_LONG).show()
                    dismiss()
                }
            }
        }

        viewModel.insertDivisionStatus.observe(viewLifecycleOwner){
            it?.let { status ->
                // If the division was inserted successfully, dismiss the bottom sheet.
                if (status != -1L) {
                    dismiss()
                }
                // Otherwise, show a snackbar message with an error message.
                else{
                    Snackbar.make(requireActivity().findViewById(android.R.id.content),
                        getString(R.string.error_inserting_division), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    // set multiple click listeners
    private fun initListeners() {
        val views = listOf(
            binding.mcvKitchen,
            binding.mcvLivingRoom,
            binding.mcvMasterBedroom,
            binding.mcvGuestRoom,
            binding.mcvCustomDivision
        )
        views.forEach { it.setOnClickListener(this) }

        binding.formAddCustomDivision.mbAddDivision.setOnClickListener {
            if (binding.formAddCustomDivision.tiNewDivisionName.text.toString().isNotEmpty()) {
                binding.formAddCustomDivision.tilNewDivisionName.error = null
                viewModel.addCustomDivision(EntityDivision(
                    divisionName = binding.formAddCustomDivision.tiNewDivisionName.text.toString(),
                    lightStatus = false ))
            } else {
                binding.formAddCustomDivision.tilNewDivisionName.error = getString(R.string.error_empty_field)
            }
        }
    }

    // Set the desired height of the BottomSheet
    private fun setPeekHeight(bottomSheet: View?) {
        // Set the desired height of the BottomSheet here
        val behavior = BottomSheetBehavior.from(bottomSheet!!)
        val displayMetrics = DisplayMetrics()
        (requireActivity().windowManager.defaultDisplay).getMetrics(displayMetrics)
        val screenHeight = displayMetrics.heightPixels
        val layoutParams = bottomSheet.layoutParams
        layoutParams.height = (screenHeight * 0.9).toInt()
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    //listens to clicks on multiple views and performs different actions based on the view clicked.
    override fun onClick(p0: View?) {
        when (p0?.id) {
            binding.mcvKitchen.id -> {
                viewModel.addDefaultDivision(DivisionType.KITCHEN)
            }
            binding.mcvLivingRoom.id -> {
                viewModel.addDefaultDivision(DivisionType.LIVING_ROOM)
            }
            binding.mcvMasterBedroom.id -> {
                viewModel.addDefaultDivision(DivisionType.MASTER_BEDROOM)
            }
            binding.mcvGuestRoom.id -> {
                viewModel.addDefaultDivision(DivisionType.GUEST_BEDROOM)
            }
            binding.mcvCustomDivision.id -> {
                toggleCustomDivisionForm()
            }
        }
    }

    //toggle the visibility of the custom division form when the corresponding button is clicked.
    private fun toggleCustomDivisionForm() {
        val currentColor = binding.mcvCustomDivision.cardBackgroundColor.defaultColor
        val newColor = if (currentColor == ContextCompat.getColor(requireContext(), R.color.purple_200)) {
            Color.WHITE.also { binding.formAddCustomDivision.root.visibility = View.GONE }
        } else {
            ContextCompat.getColor(requireContext(), R.color.purple_200).also {
                binding.formAddCustomDivision.root.visibility = View.VISIBLE
            }
        }
        binding.mcvCustomDivision.setCardBackgroundColor(newColor)
    }
}