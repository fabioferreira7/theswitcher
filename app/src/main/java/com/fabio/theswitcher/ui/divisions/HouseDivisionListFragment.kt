package com.fabio.theswitcher.ui.divisions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fabio.theswitcher.data.adapter.DivisionsAdapter
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.databinding.FragmentHouseDivisionListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 * Use the [HouseDivisionListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class HouseDivisionListFragment : Fragment() {
    lateinit var binding: FragmentHouseDivisionListBinding
    private val viewModel: HouseDivisionListViewModel by viewModels()
    lateinit var divisionsAdapter: DivisionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         divisionsAdapter =
             DivisionsAdapter(::onSwitchClicked, ::onDivisionDeleted, ::onDivisionClicked)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
       return FragmentHouseDivisionListBinding.inflate(inflater, container, false).also {
           binding = it
       }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        observeViewModel()
    }

    private fun setupRecyclerView() {
        binding.rvHouseDivisions.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            this.adapter = divisionsAdapter
        }
    }

    private fun observeViewModel() {
       lifecycleScope.launch {
           viewModel.allDivisions.distinctUntilChanged().collect{ divisions ->
               if (divisions.isEmpty()) {
                   binding.tvAddDivision.visibility = View.VISIBLE
                   binding.rvHouseDivisions.visibility = View.GONE
                   // submit an empty list to remove all items
                   divisionsAdapter.submitList(emptyList())
               }
               else {
                   binding.tvAddDivision.visibility = View.GONE
                   binding.rvHouseDivisions.visibility = View.VISIBLE
                   divisionsAdapter.submitList(divisions)
               }
           }
       }
    }

    private fun onSwitchClicked(division: EntityDivision, isChecked: Boolean) {
        viewModel.updateDivision(division.copy(lightStatus = isChecked))
    }

    private fun onDivisionDeleted(division: EntityDivision) {
        viewModel.deleteDivision(division)
    }
    private fun onDivisionClicked(division: EntityDivision) {
       findNavController().navigate(
           HouseDivisionListFragmentDirections.actionHouseDivisionListFragmentToDivisionDetailFragment(
               division.divisionName,division.lightStatus)
       )
    }


}