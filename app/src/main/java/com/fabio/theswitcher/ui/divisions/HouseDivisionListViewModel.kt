package com.fabio.theswitcher.ui.divisions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.data.repository.DivisionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HouseDivisionListViewModel @Inject constructor(private val repository: DivisionRepository) :
    ViewModel()
{

    // Exposes a Flow of all divisions from the repository for the UI to observe
    val allDivisions: Flow<List<EntityDivision>> = repository.getAllDivisions()

    // to delete given division from database
    fun updateDivision(division: EntityDivision) {
        viewModelScope.launch {
            repository.updateDivision(division)
        }
    }

    // to delete given division from database
    fun deleteDivision(division: EntityDivision) = viewModelScope.launch {
        repository.deleteDivision(division)
    }

}