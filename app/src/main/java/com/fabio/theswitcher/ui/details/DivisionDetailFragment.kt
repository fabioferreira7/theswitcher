package com.fabio.theswitcher.ui.details

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.navigation.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.fabio.theswitcher.R
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.databinding.FragmentDivisionDetailBinding
import com.fabio.theswitcher.ui.MainActivity
import com.fabio.theswitcher.ui.divisions.HouseDivisionListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DivisionDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class DivisionDetailFragment : Fragment() {

    lateinit var binding: FragmentDivisionDetailBinding
    val args: DivisionDetailFragmentArgs by navArgs()
    var lightStatus:Boolean = false
    private val viewModel: DivisionDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return FragmentDivisionDetailBinding.inflate(inflater, container, false).also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        (requireActivity() as MainActivity).updatePlusButtonVisibility(false)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        binding.ivPower.setOnClickListener {
            lightStatus = !lightStatus
            viewModel.updateDivision(EntityDivision(args.divisionName, lightStatus))
            setBackground()
        }
    }

    private fun initViews() {

        lightStatus = args.lightStatus
        (activity as AppCompatActivity).supportActionBar?.title = args.divisionName

        binding.tvDescription.text = getString(R.string.division_light_status_description,
            args.divisionName
        )
        setBackground()
    }

    fun setBackground(){
        if (lightStatus){
            binding.ivPower.setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN)
            binding.tvLightStatusValue.text = getString(R.string.on)
            Glide.with(requireContext())
                .load(R.drawable.ic_light_on)
                .centerCrop()
                .into(binding.ivLightStatus)
        }
        else{
            binding.ivPower.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
            binding.tvLightStatusValue.text = getString(R.string.off)
            Glide.with(requireContext())
                .load(R.drawable.ic_light_off)
                .centerCrop()
                .into(binding.ivLightStatus)
        }
    }
}