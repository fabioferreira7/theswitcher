package com.fabio.theswitcher.ui.bottomSheet

import android.content.res.Resources
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fabio.theswitcher.R
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.data.repository.DivisionRepository
import com.fabio.theswitcher.ui.bottomSheet.AddDivisionBottomSheetFragment.Companion.TAG
import com.fabio.theswitcher.utils.DivisionType
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddDivisionBottomSheetViewModel @Inject constructor(private val repository: DivisionRepository) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    var insertDivisionStatus = MutableLiveData<Long>()

    // Coroutine function to add a default division of a given type
    fun addDefaultDivision(divisionType: DivisionType) {
       viewModelScope.launch {
           try {
               val division = EntityDivision(
                   divisionName = divisionType.displayName,
                   lightStatus = false)
               insertDivisionStatus.value = repository.addNewDivision(division)
           } catch (e: SQLiteConstraintException) {
               errorMessage.value = "$divisionType/${divisionType.displayName}"
           }

       }
    }

    // Coroutine function to add a custom division with a given name and light status
    fun addCustomDivision(division: EntityDivision) {
        viewModelScope.launch {
            try {
                insertDivisionStatus.value = repository.addNewDivision(division)
            } catch (e: SQLiteConstraintException) {
                errorMessage.value = "${DivisionType.CUSTOM}/${division.divisionName}"
            }
        }
    }
}