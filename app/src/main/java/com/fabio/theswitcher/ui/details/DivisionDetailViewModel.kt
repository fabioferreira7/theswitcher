package com.fabio.theswitcher.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.data.repository.DivisionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DivisionDetailViewModel @Inject constructor(private val repository: DivisionRepository) :
    ViewModel()
{

    // updated when user changes the light status of a division
    fun updateDivision(division: EntityDivision) {
        viewModelScope.launch {
            repository.updateDivision(division)
        }
    }
}