package com.fabio.theswitcher.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.fabio.theswitcher.R
import com.fabio.theswitcher.databinding.ActivityMainBinding
import com.fabio.theswitcher.ui.bottomSheet.AddDivisionBottomSheetFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setup()
    }

    // Setup the toolbar with the Navigation controller and AppBarConfiguration
    private fun setup() {
        setSupportActionBar(binding.toolbar)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val appbarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setTitle(R.string.app_name)
        binding.toolbar.setupWithNavController(navController, appbarConfiguration)
    }

    // Handle click events on the options menu items
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_add -> {
                showBottomSheet()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //Inflate main menu to show add button
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    // Show bottom sheet fragment for adding new division
    private fun showBottomSheet() {
        val bottomSheet = AddDivisionBottomSheetFragment.newInstance()
        bottomSheet.show(supportFragmentManager, AddDivisionBottomSheetFragment.TAG)
    }

    // Update the visibility of the plus button in the toolbar
    fun updatePlusButtonVisibility(visible: Boolean) {
        val plusButton = binding.toolbar.menu.findItem(R.id.action_add)
        plusButton.isVisible = visible
    }
}