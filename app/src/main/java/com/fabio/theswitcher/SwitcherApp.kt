package com.fabio.theswitcher
import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SwitcherApp : Application()