package com.fabio.theswitcher.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.fabio.theswitcher.data.local.dao.DAODivision
import com.fabio.theswitcher.data.local.entities.EntityDivision

@Database(
    entities = [EntityDivision::class], version = 1
)
abstract class AppDatabase : RoomDatabase() {

    // Defines an abstract method to get an instance of the DAO for manipulating EntityDivision objects in the database
    abstract fun divisionDAO(): DAODivision

}