package com.fabio.theswitcher.data.repository

import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.utils.DivisionType
import kotlinx.coroutines.flow.Flow

interface DivisionRepository {
    suspend fun addNewDivision(entityDivision: EntityDivision): Long
    fun getAllDivisions() : Flow<List<EntityDivision>>
    suspend fun updateDivision(division: EntityDivision)
    suspend fun deleteDivision(division: EntityDivision)
}