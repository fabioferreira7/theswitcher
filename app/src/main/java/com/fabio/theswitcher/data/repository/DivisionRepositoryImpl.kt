package com.fabio.theswitcher.data.repository

import com.fabio.theswitcher.data.local.dao.DAODivision
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.utils.DivisionType
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DivisionRepositoryImpl @Inject constructor(
    private val divisionDAO: DAODivision,
) : DivisionRepository {

    // Add a new division to the database and return its generated ID
    override suspend fun addNewDivision(entityDivision: EntityDivision): Long {
       return divisionDAO.insertDivision(entityDivision)
    }

    // Retrieve all divisions from the database and emit them as a Flow of List<EntityDivision>
    override fun getAllDivisions(): Flow<List<EntityDivision>> {
        return divisionDAO.getAllDivisions()
    }

    // Update the given division in the database
    override suspend fun updateDivision(division: EntityDivision) {
        return divisionDAO.updateDivision(division)
    }

    // Delete the given division from the database
    override suspend fun deleteDivision(division: EntityDivision) {
        return divisionDAO.deleteDivision(division)
    }

}