package com.fabio.theswitcher.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Division")
data class EntityDivision(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "divisionName") val divisionName: String,
    @ColumnInfo(name = "lightStatus") val lightStatus:Boolean,
)