package com.fabio.theswitcher.data.di

import android.content.Context
import androidx.room.Room
import com.fabio.theswitcher.data.local.AppDatabase
import com.fabio.theswitcher.data.local.dao.DAODivision


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class) // Defines the component where the module will be installed
class DatabaseModule {

  // Provides a singleton instance of the AppDatabase using Room database builder
  @Provides
  @Singleton
  fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
    return Room.databaseBuilder(
      appContext,
      AppDatabase::class.java,
      "switcher.db"
    ).build()
  }

  // Provides an instance of the DAODivision interface which is used to perform database operations on the Division entity
  @Provides
  fun provideDivisionDAO(appDatabase: AppDatabase) : DAODivision {
    return appDatabase.divisionDAO()
  }
}