package com.fabio.theswitcher.data.local.dao

import androidx.room.*
import com.fabio.theswitcher.data.local.entities.EntityDivision
import kotlinx.coroutines.flow.Flow


@Dao
interface DAODivision {

    // Inserts a new Division entity into the database and returns the row ID of the newly inserted record
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertDivision(entityDivision: EntityDivision) : Long

    // Returns all the Division entities as a Flow list
    @Query("SELECT * FROM Division")
    fun getAllDivisions(): Flow<List<EntityDivision>>

    // Updates an existing Division entity in the database
    @Update
    suspend fun updateDivision(division: EntityDivision)

    // Deletes an existing Division entity from the database
    @Delete
    suspend fun deleteDivision(division: EntityDivision)

}