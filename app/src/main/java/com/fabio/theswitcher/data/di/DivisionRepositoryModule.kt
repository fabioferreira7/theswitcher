package com.fabio.theswitcher.data.di


import com.fabio.theswitcher.data.repository.DivisionRepository
import com.fabio.theswitcher.data.repository.DivisionRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


// This module provides a binding for the DivisionRepository interface to its implementation.
@Module
@InstallIn(SingletonComponent::class)
abstract class DivisionRepositoryModule {

  // This function binds the implementation class (DivisionRepositoryImpl) to the interface (DivisionRepository)
  @Binds
  abstract fun providesDivisionRepository(impl: DivisionRepositoryImpl) : DivisionRepository

}
