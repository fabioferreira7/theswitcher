package com.fabio.theswitcher.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.databinding.ItemDivisionBinding
import java.util.*


// Adapter that binds a list of EntityDivision items to a RecyclerView
class DivisionsAdapter(
    // Callback for when the switch button is clicked for a division
    private val onSwitchClicked: (EntityDivision, Boolean) -> Unit,
    // Callback for when a division is deleted
    private val onDivisionDeleted: (EntityDivision) -> Unit,
    // Callback for when a division is clicked
    private val onDivisionClicked: (EntityDivision) -> Unit
)  :
    ListAdapter<EntityDivision, DivisionsAdapter.DivisionViewHolder>(DiffCallback()) {

    // DiffUtil callback for comparing EntityDivision items
    class DiffCallback : DiffUtil.ItemCallback<EntityDivision>() {
        override fun areItemsTheSame(oldItem: EntityDivision, newItem: EntityDivision): Boolean {
            return oldItem.divisionName == newItem.divisionName
        }

        override fun areContentsTheSame(oldItem: EntityDivision, newItem: EntityDivision): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DivisionViewHolder {
        val binding =
            ItemDivisionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DivisionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DivisionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class DivisionViewHolder(private val binding: ItemDivisionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        // Callback for when the switch button is checked/unchecked
        private val onCheckedChangeListener = CompoundButton.OnCheckedChangeListener { _, isChecked ->
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val division = getItem(position)
                onSwitchClicked(division, isChecked)
            }
        }
        // Callback for when the delete button is clicked
        private val onDeleteClickListener = View.OnClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val division = getItem(position)
                onDivisionDeleted(division)
            }
        }

        // Callback for when the division item is clicked
        private val onDivisionClickListener = View.OnClickListener {
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val division = getItem(position)
                onDivisionClicked(division)
            }
        }

        init {
            binding.smLightStatus.setOnCheckedChangeListener(null)
            binding.smLightStatus.setOnCheckedChangeListener(onCheckedChangeListener)

            binding.ivDeleteDivision.setOnClickListener(null)
            binding.ivDeleteDivision.setOnClickListener(onDeleteClickListener)

            binding.root.setOnClickListener(null)
            binding.root.setOnClickListener(onDivisionClickListener)
        }

        // Binds a division to the ViewHolder
        fun bind(division: EntityDivision) {
            binding.tvDivisionName.text = division.divisionName
            binding.smLightStatus.isChecked = division.lightStatus

        }
    }
}