package com.fabio.theswitcher

import android.database.sqlite.SQLiteConstraintException
import com.fabio.theswitcher.data.local.dao.DAODivision
import com.fabio.theswitcher.data.local.entities.EntityDivision
import com.fabio.theswitcher.data.repository.DivisionRepositoryImpl
import junit.framework.Assert.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.mockito.Mockito.*

class DivisionRepositoryImplTest {

    // Instantiate mock DAO and test division data
    private val mockDao = mock(DAODivision::class.java)
    private val testDivision = EntityDivision("kitchen", false)

    // Instantiate Repository with mock DAO
    private val repository = DivisionRepositoryImpl(mockDao)

    @Test
    fun fetchDivisionsTest() = runTest {
        // Create a flow with test data
        val flow = flowOf(listOf(testDivision))
        `when`(mockDao.getAllDivisions()).thenReturn(flow)

        // Call the function being tested
        val result = repository.getAllDivisions().first()

        // Verify that the flow contains the expected data
        assertEquals(flow.first(), result)
    }

    @Test
    fun insertDivisionTest() = runTest {
        // Set up DAO to return the inserted id
        val insertedId = 1L
        `when`(mockDao.insertDivision(testDivision)).thenReturn(insertedId)

        // Call the function being tested
        val result = repository.addNewDivision(testDivision)

        // Verify that the DAO insert function was called with the correct parameter
        verify(mockDao).insertDivision(testDivision)

        // Verify that the returned id matches the expected value
        assertEquals(insertedId, result)
    }

    @Test
    fun updateDivisionTest() = runTest {
        // Call the function being tested
        repository.updateDivision(testDivision)

        // Verify that the DAO update function was called with the correct parameter
        verify(mockDao).updateDivision(testDivision)
    }

    @Test
    fun deleteDivisionTest() = runTest {
        // Call the function being tested
        repository.deleteDivision(testDivision)

        // Verify that the DAO delete function was called with the correct parameter
        verify(mockDao).deleteDivision(testDivision)
    }
}